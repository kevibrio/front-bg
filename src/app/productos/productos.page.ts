import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { ProductoService } from '../services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: 'productos.page.html',
  styleUrls: ['productos.page.scss']
})
export class ProductosPage {
  productos = [];
  constructor(private alertController: AlertController,
    private productService: ProductoService,
    private router: Router) {
      this.initPage();
    }

  async initPage(){
        const response = await this.productService.obtainProducts();
        if(response != false){
          console.log(response);
          
          this.productos = [...response]
        }

  }
}
