import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss']
})
export class LoginPage {
  correo = ''
  password = ''
  constructor(private alertController: AlertController,
    private loginService: LoginService,
    private router: Router) {}

  async iniciarSesion(){
    console.log('dasdasdas');
    if(this.correo && this.password){
        const response = await this.loginService.verifyUser(this.correo,this.password);
        if(response == true){
          console.log('xxxx');
          
          this.router.navigateByUrl('productos')
        }
    }else{
      const alerta = await this.alertController.create({
        header:'Alerta!!',
        message:'Por favor, introduzca sus credenciales'
      });
      alerta.present();
    }
  }
}
