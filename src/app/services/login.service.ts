import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Capacitor } from '@capacitor/core';
import { Directory, Filesystem } from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { AlertController, Platform } from '@ionic/angular';

import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root',
})
export class LoginService {

  constructor(private httpClient: HttpClient,
    private alertController: AlertController,
    private storage: Storage) {}

  public async verifyUser(email,password): Promise<any>{
    return new Promise(async resolve=>{
        try{
          let response = await this.httpClient.post('http://localhost:5161/api/Cliente',{
            email,password
        },{headers: {
          'Content-Type': 'application/json',
        },}).toPromise();
        if(response && response['codigoRetorno'] == '0001'){
          this.storage.set('token',response['token'])
          resolve(true);
        }else{
          const alerta = await this.alertController.create({
            header:'Alerta!!',
          message:response && response['mensajeRetorno']?response['mensajeRetorno']:'El usuario no existe'})
          alerta.present();
          resolve(false);
        }
        }catch (error){
          console.log(error);
          const alerta = await this.alertController.create({
            header:'Alerta!!',
          message:'Ocurrió un error al intentar procesar tu info'})
          alerta.present();
          resolve(false)
        }
        
    })
  }

}
