import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Capacitor } from '@capacitor/core';
import { Directory, Filesystem } from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { AlertController, Platform } from '@ionic/angular';

import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root',
})
export class ProductoService {

  constructor(private httpClient: HttpClient,
    private alertController: AlertController,
    private storage: Storage) {}

  public async obtainProducts(): Promise<any>{
    return new Promise(async resolve=>{
        try{
            const token = await this.storage.get('token');
          let response = await this.httpClient.get('http://localhost:5161/api/Product'
          ,{headers:{'Authorization': 'Bearer '+token}}).toPromise();
        if(response && response['codigoRetorno'] == '0001'){
          resolve(response['data']);
        }else{
          const alerta = await this.alertController.create({
            header:'Alerta!!',
          message:response && response['mensajeRetorno']?response['mensajeRetorno']:'No existen productos'})
          alerta.present();
          resolve(false);
        }
        }catch (error){
          console.log(error);
          const alerta = await this.alertController.create({
            header:'Alerta!!',
          message:'Ocurrió un error al intentar procesar tu info'})
          alerta.present();
          resolve(false)
        }
        
    })
  }

}
